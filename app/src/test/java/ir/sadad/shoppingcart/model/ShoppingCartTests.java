package ir.sadad.shoppingcart.model;

import ir.sadad.shoppingcart.exception.ItemNotInCartException;
import ir.sadad.shoppingcart.exception.PositionNotAvailableException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ShoppingCartTests {

    private static final Item SHIRT = new Item("Shirt", valueOf(10.99));
    private static final Item SUNGLASSES = new Item("Sunglasses", valueOf(30.99));
    private static final Item PANTS = new Item("Pants", valueOf(20.99));
    private ShoppingCart shoppingCart;

    @BeforeEach
    void initialize() {
        shoppingCart = new ShoppingCart("Alireza");
    }

    @Test
    void shouldInitializeWithEmptyList() {

        List<Item> items = shoppingCart.getItems();
        assertThat(items).isEmpty();
    }

    @Test
    void addItem_shouldAddItemToBeginningOfList() {

        shoppingCart.addItem(SUNGLASSES);
        shoppingCart.addItem(PANTS);

        List<Item> items = shoppingCart.getItems();
        assertThat(items).containsExactlyElementsOf(List.of(PANTS, SUNGLASSES));
    }

    @Test
    void removeItem_shouldRemoveItemFromList() {
        shoppingCart.addItem(SUNGLASSES);
        shoppingCart.addItem(PANTS);

        shoppingCart.removeItem(SUNGLASSES);

        List<Item> items = shoppingCart.getItems();
        assertThat(items).doesNotContain(SUNGLASSES);
    }

    @Test
    void removeItem_shouldThrowExceptionWhenItemNotExists() {
        shoppingCart.addItem(SUNGLASSES);

        assertThatExceptionOfType(ItemNotInCartException.class)
                .isThrownBy(() -> shoppingCart.removeItem(PANTS))
                .withMessage("Item is not in the cart.");
    }

    @Test
    void getItems_returnsItemsInCorrectOrder() {
        shoppingCart.addItem(SUNGLASSES);
        shoppingCart.addItem(PANTS);
        shoppingCart.addItem(SHIRT);

        List<Item> items = shoppingCart.getItems();

        assertThat(items).containsExactlyElementsOf(List.of(SHIRT, PANTS, SUNGLASSES));
    }

    @Test
    void moveItem_shouldMoveItemToCorrectPlaceInList() {
        shoppingCart.addItem(SUNGLASSES);
        shoppingCart.addItem(PANTS);
        shoppingCart.addItem(SHIRT);

        shoppingCart.moveItem(PANTS, 1);

        List<Item> items = shoppingCart.getItems();
        assertThat(items).containsExactlyElementsOf(List.of(PANTS, SHIRT, SUNGLASSES));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 3, 4})
    void moveItem_shouldThrowExceptionWhenNewPlaceNotAvailable(int newPosition) {
        shoppingCart.addItem(SUNGLASSES);
        shoppingCart.addItem(PANTS);

        assertThatExceptionOfType(PositionNotAvailableException.class)
                .isThrownBy(() -> shoppingCart.moveItem(PANTS, newPosition))
                .withMessage("This position is not available in the list.");
    }
}