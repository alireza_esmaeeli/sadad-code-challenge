package ir.sadad.shoppingcart.model;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class ItemTests {

    @Test
    void itemsWithSameNameAndPrice_shouldBeEqual() {

        Item item1 = new Item("Basket", BigDecimal.valueOf(12.99));
        Item item2 = new Item("Basket", BigDecimal.valueOf(12.99));

        assertThat(item2).isEqualTo(item1);
    }
}