package ir.sadad.shoppingcart.exception;

public class PositionNotAvailableException extends RuntimeException {
    public PositionNotAvailableException() {
        super("This position is not available in the list.");
    }
}
