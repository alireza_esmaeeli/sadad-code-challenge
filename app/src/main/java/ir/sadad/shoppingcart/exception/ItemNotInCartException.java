package ir.sadad.shoppingcart.exception;

public class ItemNotInCartException extends RuntimeException {
    public ItemNotInCartException() {
        super("Item is not in the cart.");
    }
}
