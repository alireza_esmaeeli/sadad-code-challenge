package ir.sadad.shoppingcart.model;

import ir.sadad.shoppingcart.exception.ItemNotInCartException;
import ir.sadad.shoppingcart.exception.PositionNotAvailableException;

import java.util.LinkedList;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.util.Collections.unmodifiableList;

public class ShoppingCart {

    private final String customerName;
    private final LinkedList<Item> items;

    public ShoppingCart(String customerName) {
        this.customerName = customerName;
        this.items = new LinkedList<>();
    }

    public void addItem(Item item) {
        this.items.addFirst(item);
    }

    public void removeItem(Item item) {
        boolean existed = this.items.remove(item);

        if (FALSE.equals(existed)) {
            throw new ItemNotInCartException();
        }
    }

    public void moveItem(Item item, int newPosition) {
        if (newPosition < 1 || newPosition > this.items.size())
            throw new PositionNotAvailableException();

        removeItem(item);

        this.items.add(newPosition - 1, item);
    }

    public List<Item> getItems() {
        return unmodifiableList(this.items);
    }

    public String getCustomerName() {
        return customerName;
    }
}
